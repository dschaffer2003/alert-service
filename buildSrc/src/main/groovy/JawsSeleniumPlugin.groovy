import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.tasks.SourceSet
import org.gradle.api.plugins.JavaBasePlugin
import org.gradle.api.plugins.JavaPluginConvention
import org.gradle.api.tasks.testing.Test;

import org.gradle.plugins.ide.eclipse.model.ProjectDependency
import org.gradle.plugins.ide.eclipse.model.SourceFolder
import org.gradle.api.internal.artifacts.dependencies.DefaultProjectDependency

/**
 * Plugin for building our sub-projects which consist primarily of Java code.
 */
class JawsSeleniumPlugin implements Plugin<Project> {
	void apply(Project project) {
		configureWebTests(project)
		project.afterEvaluate { p -> configureEclipse(p) }
	}

	void configureWebTests(project) {
		project.configure(project) {
			sourceSets {
				webTest
			}

			tasks.create(name: 'webTest', type: Test) {
				testClassesDir = sourceSets.webTest.output.classesDir
				classpath = sourceSets.webTest.runtimeClasspath
				reports.html.destination = file('build/reports/webTests')

				ignoreFailures = ignoreTestFailures.equalsIgnoreCase('true')

				// set the web browser with -PunittestBrowser=firefox when running gradle
				systemProperty 'unittest.browser', unittestBrowser
			}


//	repositories {
//		maven {
//			// jboss distributes a new version of webserver.jar that is not quite compatible
//			url "https://repository.jboss.org/nexus/content/repositories/thirdparty-releases"
//		}
//	}

			dependencies {

				// Not available from maven central:
				// TODO: depending on a project here is bad
				webTestCompile files("${project.project(':server:rest').projectDir}/test/lib/webserver.jar")
//		webTestCompile group: 'tjws', name: 'webserver', version: '1.3.3' // this is not trivially compatible

				webTestCompile 'junit:junit:4.11'

				// Note: 2.35 or newer required for Firefox 23 and later (see https://code.google.com/p/selenium/issues/detail?id=6051)
				webTestCompile (group: 'org.seleniumhq.selenium', name: 'selenium-java', version: '2.43.1')
			}
		}
	}
	
	void configureEclipse(project) { 
		project.configure(project) {
			eclipse.classpath {
				plusConfigurations += [ project.configurations.webTestRuntime ]
			}
		}
	}
}

