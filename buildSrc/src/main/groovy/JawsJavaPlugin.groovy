import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.internal.artifacts.dependencies.DefaultProjectDependency
import org.gradle.api.tasks.testing.Test
import org.gradle.plugins.ide.eclipse.model.ProjectDependency
import org.gradle.plugins.ide.eclipse.model.Container
import org.gradle.plugins.ide.eclipse.model.SourceFolder
import org.gradle.testing.jacoco.tasks.JacocoReport


/**
 * Plugin for building our sub-projects which consist primarily of Java code.
 */
class JawsJavaPlugin implements Plugin<Project> {
	void apply(Project project) {
		// println 'Applying JawsJavaPlugin to ' + project.name

		// By default, we make auto-boxing and -unboxing an error in Eclipse, 
		// but this property allows it to be overridded at the project level:
		project.ext.forbidAutoBoxing = true
		
		// By default, warn if any package doesn't have the file where we put the 
		// default annotation for FindBugs:
		project.ext.missingPackageInfoWarningsEnabled = true
		
		configureJava(project)
		configureUnitTests(project)
		
		configureIntegTest(project, 'integTest')
		configureIntegTest(project, 'perfTest')

		configureQaIntegTest(project, 'qaIntegTest')
		configureQaIntegTest(project, 'qaFullRegressionTest')
		
		patchCompilerOutput(project)
		
		configureFindBugs(project)
		
		project.afterEvaluate { p -> configureEclipse(p) }
	}
	
	def configureJava(Project project) {
		
        project.configure(project) {
			apply plugin: 'java'
	        apply plugin: 'jacoco'
	        apply plugin: 'eclipse'
	        apply plugin: "org.zeroturnaround.gradle.jrebel"

	        repositories {
		        maven {
					url 'http://tslmaven/repository/terma/'
				}
				mavenCentral()
			}
			
			dependencies {
				compile 'com.google.code.findbugs:findbugs-annotations:3.0.1'
				// Not able to pull from mavenCentral:
				compile files("$project.rootProject.projectDir/lib/jsr305.jar")
				
				testCompile 'junit:junit:4.11'
				testCompile 'org.hamcrest:hamcrest-library:1.3'
			}
			
			def compilerArgs = []

 			if (showWarnings.equalsIgnoreCase('true')) {
//				compileJava.options.compilerArgs = ['-Xlint', '-Xlint:-serial', '-Xlint:-path']
				// "-path" because there were tons of mysterious missing jars, and anyway that's
				//    a build problem and not a code problem, so who cares?
				// "-serial" because we don't believe in serialVersionUID! DTOs should either 
				//    match or fail! (or, at least, that's was the original thinking...)
				compilerArgs.addAll(['-Xlint', '-Xlint:-serial', '-Xlint:-path'])
 			}
			 
			//compilerArgs.add(['-XprintRounds', '-xprintProcessorInfo'])  // print information about annotation processors

			// Always specify a location for annotation-processor-generated source, 
			// even though only a few projects will use it:
			compilerArgs.addAll([ '-s', "$project.buildDir/apt" ])
			compileJava.doFirst {
				mkdir 'build/apt'
			}

			compileJava.options.compilerArgs = compilerArgs 
			
			// Gets included in the name of each jar:
			version = rootProject.version
		
			// Prefix each jar with "jaws-", and use the entire path, to reduce 
			// confusion when hassembling the ear, etc.:
			archivesBaseName = "jaws${path.replace(':', '-')}"

	        def buildId = rootProject.buildId
	        if (rootProject.versionQualifier.contains('dev')) {
		        buildId = buildId.substring(0, buildId.lastIndexOf('-') + 9)
	        }

	        jar {
				manifest {
					attributes 'provider': 'Terma Software Labs'
					attributes 'version': version
					attributes 'build-id': buildId
				}
		        dependsOn generateRebel
			}

	        rebel {
		        classpath {
			        resource {
				        directory = "build/eclipse"
			        }

			        resource{} // gradle & intellij build dirs.  Preempted by eclipse build dirs.
		        }
	        }

			tasks.test.reports.html.destination = file('build/reports/test')
			
			javadoc {
				title = "JAWS ${project.path.substring(1).replace(':', '-')} API ($version)"
				
				// Wire cross-project links for some classes that are commonly
				// used in method signatures (such as EpochTime, et al. and
				// the POJOs and "Lite" objects). It appears that the task
				// already dependsOn the referenced projects' javadoc tasks.
				// Tricky: dependencies aren't configured until after this plugin
				// is applied, so we need to wait until the task is ready to run 
				// to inspect them and add links.
				doFirst {
					options {
						logger.info('Adding javadoc links for project dependencies:')
						def compileConfig = project.configurations.find { it.name == 'compile' }
						compileConfig.dependencies.each { d ->
							if (d instanceof DefaultProjectDependency) {
								def p = d.dependencyProject
								def javadocTask = p.tasks.findByName('javadoc')
								if (javadocTask.enabled) {
									def javadocDir = javadocTask.destinationDir
									if (javadocDir.exists()) {
										logger.info("  $javadocDir")
										links javadocDir.path
									}
									else {
										logger.info("  (missing--no source?) $javadocDir")
									}
								}
								else {
									logger.info("  (disabled) $javadocTask")
								}
							}
						}
					}
				}
			}
	
			// TODO: not sure how many of these files exist, but it might be worth 
			// getting this going.		
//			<!-- Copy embedded supporting files, stripping off the "doc" directory on the way. -->
//			<copy todir="${build.javadoc.dir}" verbose="true">
//				<fileset dir="${src.client.dir}" includes="**/doc/*" />
//				<mapper type="regexp" from="^(.*)\\|/doc\\|/(.*)$$" to="\1/\2" />
//			</copy>
			
			tasks.create(name: 'checkPackageInfoExists') {
				description = 'For projects that use auto-boxing, make sure every source package has a file called package-info.java.'
				
				doLast {
					if (!forbidAutoBoxing && !missingPackageInfoWarningsEnabled) {
						logger.warn('Warning: auto-boxing enabled and missing package-info warnings disabled. This is asking for trouble!')
					}
					
					def filesByPkg = [:]
					sourceSets.main.allJava.each { f ->
						def m = f.path =~ /.*(com\/termalabs\/.*)\/([^\/]+\.java)/
						if (m) {
							def pkg = m[0][1]
							def cl = m[0][2]
							if (filesByPkg[pkg] == null) filesByPkg[pkg] = [] as Set
							filesByPkg[pkg].add(cl)
						}
					}
					filesByPkg.keySet().each { pkg ->
						if (!filesByPkg[pkg].contains('package-info.java')) {
							if (forbidAutoBoxing && !missingPackageInfoWarningsEnabled) {
								// Note: logging at quiet means you see it even if -q is used
								logger.info("No package-info.java file in package: $pkg (but auto-boxing is forbidden or the warning is disabled)")
							}
							else {
								logger.warn("Warning: no package-info.java file in package: $pkg")
							}
						}
						else {
							logger.info("Found package-info.java in package: $pkg")
						}
					}
				}
			}
			
			tasks.compileJava.dependsOn checkPackageInfoExists
        }
	}
	
	void configureUnitTests(Project project) {
		project.configure(project) {
			tasks.test {
				ignoreFailures = ignoreTestFailures.equalsIgnoreCase('true')
				// Don't do code coverage unless -Pcodecoverage is used in commandline
				jacoco {
					enabled = project.hasProperty("codecoverage")
				}
			}
		}
	}

	void configureIntegTest(Project project, String name) {
		createTestTask(project, name)
		createJacocoReport(project, name)
        project.tasks.check.dependsOn name
	}
	
	void configureQaIntegTest(Project project, String name) {
        createTestTask(project, name)
		createJacocoReport(project, name)
	}
	
	void createTestTask(Project project, String name) {
		project.configure(project) {
			sourceSets.create(name)
			
			sourceSets.getByName(name).compileClasspath += sourceSets.main.runtimeClasspath
			sourceSets.getByName(name).compileClasspath += sourceSets.test.runtimeClasspath
			sourceSets.getByName(name).runtimeClasspath += sourceSets.main.runtimeClasspath
			sourceSets.getByName(name).runtimeClasspath += sourceSets.test.runtimeClasspath
		
			tasks.create(name: name, type: Test) {
				testClassesDir = sourceSets.getByName(name).output.classesDir
				classpath = sourceSets.getByName(name).runtimeClasspath
				reports.html.destination = file('build/reports/' + name)
				ignoreFailures = ignoreTestFailures.equalsIgnoreCase('true')
				
				// Don't do code coverage unless i.e. -Pcodecoverage is used in commandline
				jacoco {
					enabled = project.hasProperty('codecoverage')
				}
				
				//set defaults for jvm parameters in case none are passed
			if(!project.hasProperty("hostServer")){
				project.ext.set("hostServer", "devlja01")
			}
			if(!project.hasProperty("user")){
				project.ext.set("user", "root")
			}
			if(!project.hasProperty("dbType")){
				project.ext.set("dbType", "oracle")
			}
			if(!project.hasProperty("machineType")){
				project.ext.set("machineType", "linux")
			}
			if(!project.hasProperty("runExcluded")){
				project.ext.set("runExcluded", "false")
			}
			if(!project.hasProperty("runOne")){
				project.ext.set("runOne", "false")
			}		
			if(!project.hasProperty("dbHost")){
				project.ext.set("dbHost", "qalora03")
			}

			//set jvm arguments to pass to jython test scripts
			jvmArgs '-DhostServer='+project.getProperty("hostServer")
			jvmArgs '-Duser='+project.getProperty("user")
			jvmArgs '-DdbType='+project.getProperty("dbType")
			jvmArgs '-DmachineType='+project.getProperty("machineType")
			jvmArgs '-DrunExcluded='+project.getProperty("runExcluded")
			jvmArgs '-DrunOne='+project.getProperty("runOne")
			jvmArgs '-DdbHost='+project.getProperty("dbHost")
							
			//System.setProperty('hostServer', project.ext.hostServer)	
			//System.setProperty('user', project.ext.user)
			//System.setProperty('runOne', project.ext.runOne)
			//System.setProperty('runOne', System.getProperty('runOne', project.ext.runOne))

			}
        }
	}
	
	void createJacocoReport(Project project, String name) {
		project.configure(project) {
			tasks.create(name: 'jacoco' + upperFirstLetter(name) + 'Report', type: JacocoReport) {
				sourceSets sourceSets.main
				executionData sourceSets.getByName(name)
		    }
	    }
	}
	
	String upperFirstLetter(String string) { 
		return string.substring(0, 1).toUpperCase() + string.substring(1)
	}

	
	def patchCompilerOutput(project) {
		// Hack to fix a problem where Hudson runs lines together in
		// the build log. Seems to happen whenever you pipe stderr and stdout
		// to the same stream.
		// It probably would be enough to flush the logger's stream, if I knew 
		// how to get ahold of it.
		project.configure(project) {
			compileJava.doFirst {
				logger.lifecycle('')
			}
			compileTestJava.doFirst {
				logger.lifecycle('')
			}
			compilePerfTestJava.doFirst {
				logger.lifecycle('')
			}
			compileIntegTestJava.doFirst {
				logger.lifecycle('')
			}
			compileQaIntegTestJava.doFirst {
				logger.lifecycle('')
			}
			compileQaFullRegressionTestJava.doFirst {
				logger.lifecycle('')
			}
		}
	}

	// method called afterEvaluate, since we modify runtimeConfigurations in subproject gradle files and we should
	// include these modifications in the eclipse classpath's plusConfigurations
	def configureEclipse(Project project) {
		// For now, all our Java sub-projects (and only Java ones) get the
		// same eclipse config. 
		project.configure(project) {		
			eclipse.project {
				name = nameForProject(project)
		
				natures 'org.springsource.ide.eclipse.gradle.core.nature'
			}
			
			eclipse.classpath {
				defaultOutputDir = file('build/eclipse')
				
				// Note: by default you get source but not javadoc
				downloadJavadoc = true
				
				// Add the dependencies of our additional configurations: 
				plusConfigurations += [ project.configurations.perfTestRuntime ]
				plusConfigurations += [ project.configurations.integTestRuntime ]
				plusConfigurations += [ project.configurations.qaIntegTestRuntime ]
				plusConfigurations += [ project.configurations.qaFullRegressionTestRuntime ]
				
				// Intercept the classpath after it's constructed, to fix up a 
				// couple of things that it isn't doing right for us:  
				file.whenMerged { classpath ->
					classpath.entries.each { entry ->
						if (entry instanceof ProjectDependency) {
							// Use our expanded project name:
							def depName = entry.path.substring(1) 
							// HACK: two projects have '-' in the name
							if (!depName.contains('-') || depName.startsWith('ca7-')) {
								def dep = findDependency(project, depName)
								entry.path = '/' + nameForProject(dep)
							}
							else {
								logger.info("Project dependency not mapped, appears to be resolved already: $depName")
							}
						}
						else if (entry.kind == 'con') { // set project jre lib to be the workspace default
							entry.path = "org.eclipse.jdt.launching.JRE_CONTAINER"
						}
					}
					
					def totalEntries = classpath.entries.size()
					removeDupes(classpath.entries) { e -> e instanceof SourceFolder || e instanceof Container 
																? e.path : null }
					ignoreOptionalProblems(classpath.entries.findAll { it.getPath().equals("src/test/java") ||
																	   it.getPath().equals("src/integTest/java") ||
																	   it.getPath().equals("src/perfTest/java") ||
																	   it.getPath().equals("src/connectionTest/java") ||
																	   it.getPath().equals("src/otherTest/java") ||
																	   it.getPath().equals("src/qaFullRegressionTest/java")
																	 })
					def trimmedEntries = classpath.entries.size()
					if (totalEntries > trimmedEntries) {
						println "Removed ${totalEntries - trimmedEntries} redundant source folders"
					} 
				}
			}
				
			eclipse.jdt {
				file.withProperties { props ->
					props.put('org.eclipse.jdt.core.compiler.problem.autoboxing', forbidAutoBoxing ? 'error' : 'ignore')
					
					// For the time being, we want different versions of the DTOs to be incompatible:
					props.put('org.eclipse.jdt.core.compiler.problem.missingSerialVersion', 'ignore')
					
					// Warnings flagged by javac in Hudson:
					props.put('org.eclipse.jdt.core.compiler.problem.unnecessaryTypeCheck', 'warning')

					// This is also flagged by FindBugs:
					props.put('org.eclipse.jdt.core.compiler.problem.missingHashCodeMethod', 'error')
					
					// Flag bad javadoc references and tags: 
					props.put('org.eclipse.jdt.core.compiler.doc.comment.support', 'enabled')
					props.put('org.eclipse.jdt.core.compiler.problem.invalidJavadoc', 'error')
					props.put('org.eclipse.jdt.core.compiler.problem.invalidJavadocTags', 'enabled')
					props.put('org.eclipse.jdt.core.compiler.problem.invalidJavadocTagsNotVisibleRef', 'enabled')
					props.put('org.eclipse.jdt.core.compiler.problem.invalidJavadocTagsVisibility', 'private')
				}
			}
        }
	}
		
	/**
	 * So that autoboxing errors are ignored for test code
	 */
	def ignoreOptionalProblems(List<SourceFolder> sourceFolders) {
	    sourceFolders.forEach( { e -> ignoreOptionalProblems(e) } )
    }
    
    def ignoreOptionalProblems(SourceFolder sourceFolder) {
		def attributes = sourceFolder.getEntryAttributes() 
  		attributes.put("ignore_optional_problems", "true") 
  	}
  	
	/** 
	 * Search for a project with a certain simple name. 
	 */
	Project findDependency(Project project, String simpleName) {
		// Note: need to use the project's directory, because we override the name
		// to include the path, essentially. 
		// For example, if the root poject is "jaws-gradle", then the name of the 
		// project found in common/util is "jaws-gradle-common-util". 
		def matches = new ArrayList(project.rootProject.allprojects.findAll { it.projectDir.name == simpleName })
		if (matches.size() < 1) {
			throw new IllegalArgumentException("No project matching simple name: $simpleName")
		} 
		else if (matches.size() > 1) {
			throw new IllegalArgumentException("Found multiple projects matching simple name: $simpleName; ${matches.join(', ')}")
		}
		else {
			def p = matches[0]
			project.logger.info("Matched project dependency: $simpleName -> $p.name")
			return p
		}
	}
	
	/**
	 *  Use both the root project name and the whole path in the project name. 
	 */
	String nameForProject(Project project) {
		project.rootProject.name + project.path.replace(':', '-')
	}
	
	/**
	 * Filter a collection, applying a closure to each element and keeping only
	 * the first element for each unique result value (by destructively updating 
	 * the collection). Seems like there would be a way to do this using 
	 * existing list primitives...
	 */
	def removeDupes(Iterable c, Closure closure) {
		def seen = new HashSet()
		for (Iterator iter=c.iterator(); iter.hasNext(); ) {
			def elem = iter.next()
			def result = closure(elem)
			if (result != null && !seen.add(result)) {
				iter.remove()
			}
		}
	}

	
	def configureFindBugs(Project project) {
		if (project.hasProperty('excludeFromFindbugsAnalysis')) {
			return;
		}

		if (project.findBugsReportFormat != 'xml' && project.findBugsReportFormat != 'html') throw new Exception("Expected 'html' or 'xml' for findBugsFormat, found: " + project.findBugsReportFormat)

		project.configure(project) {
			apply plugin: 'findbugs'
			
			// See: http://www.gradle.org/docs/current/dsl/org.gradle.api.plugins.quality.FindBugsExtension.html
			
			findbugs {
				// It would be nice to omit this line and go to a more current 
				// version, but in the interest of getting things running, this 
				// is the version we used previously: 
				toolVersion = '3.0.1'

//				reportLevel = 'high' // Need to switch to high and maybe turn off some warnings.
				reportLevel = 'medium'
				effort = 'max'

				sourceSets = [ sourceSets.main ]
				excludeFilter = file("${rootProject.projectDir}/tools/findbugs/exclude.xml") // HACK?
				ignoreFailures = true  // don't fail the build, for Hudson's sake
			}
			
			
			findbugsMain {
				reports {
					xml.enabled = project.findBugsReportFormat == 'xml'
					html.enabled = project.findBugsReportFormat == 'html'
				}
			}

			// Note: no need to disable these tasks, because we configure the "check"
			// task to only run findbugsMain. If you want to run these other ones,
			// you have to specifically request them. 
//			findbugsTest.enabled = false
//			findbugsPerfTest.enabled = false
//			findbugsIntegTest.enabled = false
//			findbugsQaIntegTest.enabled = false
//			findbugsQaFullRegressionTest.enabled = false
		}
	}
}

