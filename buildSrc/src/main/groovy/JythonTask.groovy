/*
 * Created on Jun 11, 2013
 * 
 * Copyright (C) 2013 Terma Software Labs
 */

import org.gradle.api.DefaultTask
import org.gradle.api.internal.file.FileResolver
import org.gradle.api.tasks.TaskAction
import org.gradle.process.internal.DefaultJavaExecAction

class JythonTask extends DefaultTask {
	String script
	Iterable<String> args = []
	Iterable<String> jvmArgs = []
	def classpath
	def pythonPath
    def workingDir
	
	@TaskAction
	def run() {
		// Creating a JavaExecAction allows a new classpath to be specified:
		def action = new DefaultJavaExecAction(getServices().get(FileResolver.class))
		
		action.jvmArgs jvmArgs
		if (pythonPath != null) {
			action.jvmArgs "-Dpython.path=$pythonPath"
		}
		
		action.main = 'org.python.util.jython'
		action.args script
		action.args args
		
		if (classpath != null) {
			action.classpath += classpath
		}
		else if (project.hasProperty('sourceSets')){
			action.classpath += project.sourceSets.main.runtimeClasspath
		}

        if (workingDir != null) {
            action.workingDir = workingDir
        }
		
		// TODO: somehow pull jython from maven central, and refer to the cached jar here?
		action.classpath += project.files("$project.rootProject.projectDir/tools/jython2.7.0/jython.jar")
		
		action.execute()
	}
}
