import org.gradle.api.Action
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.InputDirectory
import org.gradle.api.tasks.OutputDirectory
import org.gradle.api.tasks.TaskAction
import org.gradle.api.tasks.incremental.IncrementalTaskInputs

/**
 * Incremental task that signs any JARs that have changed since the last build.
 * Note: currently we have to copy the un-signed jars into a single directory
 * so that the IncrementalTaskInputs magic will work. 
 */
class SignJarsTask extends DefaultTask {
	@InputDirectory
	File inputDir
	
	@OutputDirectory
	File outputDir
	
	@TaskAction
	void execute(IncrementalTaskInputs inputs) {
		inputs.outOfDate({ change ->
			def src = change.file
			project.copy {
				from (src) { 
					include '**/*.jar'
				}
				into outputDir
			}
	
			logger.lifecycle("  signing $src.name")
			def dest = project.file("$outputDir/$src.name")
			// TODO: figure out how to get the output from ant.signjar to go to 
			// "quiet" level, so we'll see teh warning about certificate expiration.
			// We timestamp using https://timestamp.geotrust.com/tsa which is recommended by Thawte.
			// see https://www.thawte.com/resources/getting-started/code-signing-faq/index.html
			// Without timestamping, when signing certificate expires the signature will no longer
			// be valid.  You can see whether the timestamping works by running
			//	$ jarsigner -verify jaws-common-ejbclient-4.3.5.jar
			//	jar verified.
			//
			//	Warning:
			//	This jar contains entries whose signer certificate will expire within six months.
			//	This jar contains signatures that does not include a timestamp. Without a timestamp,
			//  users may not be able to validate this jar after the signer certificate's expiration
			//  date (2015-08-20) or after any future revocation date.
			//
			//	Re-run with the -verbose and -certs options for more details.
	
            ant.signjar(jar: dest,
                    alias: project.property('keystore.alias'),
                    keypass: project.property('key.pass'),
                    keystore: new File(project.rootProject.projectDir, project.property('keystore.name')),
                    storepass: project.property('keystore.pass'),
		            tsaurl:project.property('timestamp.url'),
		            executable:project.property('jarsigner.executable')
            )
		} as Action)
		
		inputs.removed({ change -> 
			def targetFile = project.file("$outputDir/${change.file.name}")
			if (targetFile.exists()) {
				targetFile.delete()
			}
	
		} as Action)
	}
}