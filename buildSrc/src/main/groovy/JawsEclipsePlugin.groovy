import org.gradle.api.Plugin

import org.gradle.api.Project
import org.gradle.api.tasks.SourceSet
import org.gradle.api.plugins.JavaBasePlugin
import org.gradle.api.plugins.JavaPluginConvention

import org.gradle.plugins.ide.eclipse.model.ProjectDependency
import org.gradle.plugins.ide.eclipse.model.SourceFolder

/**
 * Plugin for every project that we want to add to Eclipse (which is all of them, 
 * basically).
 * <p>
 * We override the project name to make it unique in the presence of multiple 
 * base projects, and add gradle nature.
 */
class JawsEclipsePlugin implements Plugin<Project> {
	void apply(Project project) {
        project.configure(project) {
			repositories {
				mavenCentral()
			}

			version = rootProject.version
			
			apply plugin: 'eclipse'
			
			eclipse.project {
				name = nameForProject(project)
		
				natures 'org.springsource.ide.eclipse.gradle.core.nature'
			}

			eclipse.classpath {
				// Note: this currently seems to result in _two_ out directories
				// in the generated .classpath, but Eclipse seems to do the right 
				// thing anyway.
				defaultOutputDir = file('build/eclipse')
			}
        }
	}
	
	/**
	 *  Use both the root project name and the whole path in the project name.
	 */
	String nameForProject(Project project) {
		project.rootProject.name + project.path.replace(':', '-')
	}

}

