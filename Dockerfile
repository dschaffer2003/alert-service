FROM tomcat:slim
RUN sed "s/8080/8081/g" < /usr/local/tomcat/conf/server.xml > /tmp/server.xml
RUN cp /tmp/server.xml /usr/local/tomcat/conf/server.xml
COPY alertsService.war /usr/local/tomcat/webapps
