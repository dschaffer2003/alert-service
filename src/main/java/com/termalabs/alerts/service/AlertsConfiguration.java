package com.termalabs.alerts.service;

import java.util.Properties;

import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.AsyncRestTemplate;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

import static com.termalabs.util.LoggerUtils.debug;
import static com.termalabs.util.LoggerUtils.warn;

@Configuration
public class AlertsConfiguration {
	
	private static final Logger logger = Logger.getLogger(AlertsConfiguration.class);

	@Bean
	public RestTemplate getRestTemplate() {
		RestTemplate restTemplate = new RestTemplate();
		// TODO Set the timeouts
//		((SimpleClientHttpRequestFactory) restTemplate.getRequestFactory())
//				.setConnectTimeout(getConnectTimeout());
//		((SimpleClientHttpRequestFactory) restTemplate.getRequestFactory())
//				.setReadTimeout((int) TimeUnit.SECONDS.toMillis(getRequestTimeout()));
		return restTemplate;
	}
	
	@Bean
	public AsyncRestTemplate getAsyncRestTemplate() {
		final SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
		requestFactory.setTaskExecutor(new SimpleAsyncTaskExecutor());
//
//		requestFactory.setConnectTimeout(getConnectTimeout());
//		requestFactory.setReadTimeout(getRequestTimeout());

		final AsyncRestTemplate asyncRestTemplate = new AsyncRestTemplate();
		asyncRestTemplate.setAsyncRequestFactory(requestFactory); 
		return asyncRestTemplate;
	}

	/*
	 * TODO Once again we are copying and pasting this.  Can we put this in a shared
	 * configurations service?
	 */
	@Bean
	public ActiveMQConnection getActiveMQConnection() {
		try {
			Properties props = new Properties();
			props.setProperty("java.naming.factory.initial", "org.apache.activemq.jndi.ActiveMQInitialContextFactory");
			props.setProperty("java.naming.provider.url", "tcp://localhost:61616");

			InitialContext ctx = new InitialContext(props);
			ConnectionFactory connectionFactory = (ConnectionFactory) ctx.lookup("ConnectionFactory");
			return (ActiveMQConnection) connectionFactory.createConnection("admin",
					"Terma123");
		}
		catch (JMSException | NamingException e) {
			try {
				warn(logger, "Unable to connect to ActiveMQ", e);
				debug(logger, "Constructing test activeMQ instance in case we are in test mode");
				ConnectionFactory connectionFactory = new ActiveMQConnectionFactory("vm://localhost?broker.persistent=false");
				return (ActiveMQConnection) connectionFactory.createConnection();
			}
			catch (JMSException e1) {
				debug(logger, "Unable to construct test activeMQ instance ", e);
				return null;
			}
		}

	}
	
	@Bean
	public ObjectMapper getObjectMapper() {
		return new ObjectMapper();
	}

}
