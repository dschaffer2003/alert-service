package com.termalabs.alerts.service;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author dschaffer
 *
 */
@RestController
public class AlertsController {
	
	private Logger logger = Logger.getLogger(getClass());
	
	public AlertsController() {
		logger.info("Starting Alerts Service");
	}

    @RequestMapping("/helloWorld")
    public String helloWorld() {
        return "Hello From Alerts Service";
    }
}
